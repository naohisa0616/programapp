//
//  ViewController.swift
//  ProgramApp
//
//  Created by 宮崎直久 on 2021/08/14.
//

import UIKit
import Intents
import FirebaseDatabase

class ViewController: UIViewController {
    
    var databaseRef: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("あ")
        
        // Firebaseの共有インスタンスを取得
        databaseRef = Database.database().reference()

        // データ保存処理後のコールバック関数
        let resultCallback = { (error: Error?, ref: DatabaseReference) -> () in
            print(#function)
        }

        // データ保存
        self.databaseRef.child("Root").childByAutoId().setValue("TestValue", withCompletionBlock: resultCallback)
    }
    
}

